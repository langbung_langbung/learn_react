import React from "react"
import CardList from "../Components/CardList";
import SearchBox from "../Components/SearchBox";
import { connect } from "react-redux";
// import {robots} from "./robots";
import "./App.css";
import Scroll from "../Components/Scroll";
import ErrorBoundry from "../Components/ErrorBoundry";
import {setSearchField,requestRobots} from "../actions";

class App extends React.Component{
    componentDidMount(){
        this.props.onRequestRobots();
    }

    render(){
        const {searchField,onSearchChange,robots,isPending} = this.props;
        const filterRobots = robots.filter(robot =>{
            return robot.name.toLowerCase().includes(searchField.toLowerCase())
        });
        return isPending?
            <h1>Loading</h1>:
             (
                <div className="tc">
                    <h1 className={"f1 AppName"}>Robo friends</h1>
                    <SearchBox searchChange={onSearchChange}/>
                    {/*<SearchBox />*/}
                    <Scroll>
                        <ErrorBoundry>
                            <CardList robots={filterRobots}/>
                            {/*<CardList robots={robots}/>*/}
                        </ErrorBoundry>
                    </Scroll>
                </div>
             );
    }
}

const mapStateToProps = (state) =>{
    return {
        searchField:state.searchRobots.searchField,
        robots:state.requestRobots.robots,
        isPending:state.requestRobots.isPending,
        error:state.requestRobots.error,
    }
}

const mapDispatchToProps = (dispatch) =>{
    return {
        onSearchChange:(event) => dispatch(setSearchField(event.target.value)),
        onRequestRobots:()=>requestRobots(dispatch)
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(App);
// export default App;
