import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from "react-redux";
import {createStore ,applyMiddleware,combineReducers} from "redux";
import thunkMiddleware from "redux-thunk";
import {createLogger} from "redux-logger";
import './index.css';
import App from './Containers/App';
import registerServiceWorker from './registerServiceWorker';
import "tachyons";
import {searchRobots,requestRobots} from "./reducers";


const logger = createLogger();
const rootReducer = combineReducers({searchRobots,requestRobots})
const store = createStore(rootReducer,applyMiddleware(thunkMiddleware,logger));
ReactDOM.render(
    <div>
        <Provider store = {store}>
            <App />
        </Provider>
    </div>
    , document.getElementById('root'));
registerServiceWorker();
