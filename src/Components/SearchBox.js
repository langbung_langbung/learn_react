import React from "react";
const SearchBox = ({searchChange}) =>{
    return(
        <div>
            <input
                className="b--black bg-light-green pa3"
                type="search"
                placeholder="Search.."
                onChange={searchChange}
            />
        </div>
    );
}
export default SearchBox
