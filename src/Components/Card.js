import React from "react";
import "./Card.css"
const Card =({name,email,id})=>{
    const divStyle =
        {
            width:"420px"
        };
    return(
        <div style={divStyle} className="bg-light-pink hover-bg-pink dib br3 pa3 ma2 grow shadow-5 card">
            <img src={`https://robohash.org/${id}?200x200`} alt="`{props.id}`"/>
            <div className="tc">
                <h1>{name}</h1>
                <p>{email}</p>
            </div>
        </div>
    );
}
export default Card
